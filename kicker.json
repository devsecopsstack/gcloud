{
  "name": "Google Cloud",
  "description": "Deploy your application to the [Google Cloud](https://cloud.google.com/) platform",
  "template_path": "templates/gitlab-ci-gcloud.yml",
  "kind": "hosting",
  "prefix": "gcp",
  "is_component": true,
  "variables": [
    {
      "name": "GCP_CLI_IMAGE",
      "description": "The Docker image used to run Google Cloud CLI commands",
      "default": "gcr.io/google.com/cloudsdktool/cloud-sdk:latest"
    },
    {
      "name": "GCP_KEY_FILE",
      "description": "Global Service Account key file",
      "secret": true,
      "mandatory": true
    },
    {
      "name": "GCP_WORKLOAD_IDENTITY_PROVIDER",
      "description": "Default [Workload Identity Provider](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) associated with GitLab to authenticate\n\n(has format `projects/PROJECT_NUMBER/locations/global/workloadIdentityPools/POOL_ID/providers/PROVIDER_ID`)",
      "advanced": true
    },
    {
      "name": "GCP_SERVICE_ACCOUNT",
      "description": "Default Service Account to which impersonate with WIF authentication",
      "advanced": true
    },
    {
      "name": "GCP_BASE_APP_NAME",
      "description": "Base application name",
      "default": "$CI_PROJECT_NAME",
      "advanced": true
    },
    {
      "name": "GCP_ENVIRONMENT_URL",
      "type": "url",
      "description": "The default environments url _(only define for static environment URLs declaration)_\n\n_supports late variable expansion (ex: `https://%{environment_name}.gcloud.acme.com`)_"
    },
    {
      "name": "GCP_SCRIPTS_DIR",
      "description": "Directory where Google Cloud scripts (deploy & cleanup) are located",
      "default": ".",
      "advanced": true
    }
  ],
  "features": [
    {
      "id": "review",
      "name": "Review",
      "description": "Dynamic review environments for your topic branches (see GitLab [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/))",
      "variables": [
        {
          "name": "GCP_REVIEW_PROJECT",
          "description": "Google Cloud project ID for review env",
          "mandatory": true
        },
        {
          "name": "GCP_REVIEW_APP_NAME",
          "description": "The application name for review env (only define to override default)",
          "advanced": true
        },
        {
          "name": "GCP_REVIEW_AUTOSTOP_DURATION",
          "description": "The amount of time before GitLab will automatically stop `review` environments",
          "default": "4 hours"
        },
        {
          "name": "GCP_REVIEW_ENVIRONMENT_URL",
          "type": "url",
          "description": "The review environments url _(only define for static environment URLs declaration and if different from default)_",
          "advanced": true
        },
        {
          "name": "GCP_REVIEW_KEY_FILE",
          "description": "Service Account key file to authenticate on review env (only define to override default)",
          "secret": true
        }
      ]
    },
    {
      "id": "integration",
      "name": "Integration",
      "description": "A continuous-integration environment associated to your integration branch (`develop` by default)",
      "variables": [
        {
          "name": "GCP_INTEG_PROJECT",
          "description": "Google Cloud project ID for integration env",
          "mandatory": true
        },
        {
          "name": "GCP_INTEG_APP_NAME",
          "description": "The application name for integration env (only define to override default)",
          "advanced": true
        },
        {
          "name": "GCP_INTEG_ENVIRONMENT_URL",
          "type": "url",
          "description": "The integration environment url _(only define for static environment URLs declaration and if different from default)_",
          "advanced": true
        },
        {
          "name": "GCP_INTEG_KEY_FILE",
          "description": "Service Account key file to authenticate on integration env (only define to override default)",
          "secret": true
        }
      ]
    },
    {
      "id": "staging",
      "name": "Staging",
      "description": "An iso-prod environment meant for testing and validation purpose on your production branch (`main` or `master` by default)",
      "variables": [
        {
          "name": "GCP_STAGING_PROJECT",
          "description": "Google Cloud project ID for staging env",
          "mandatory": true
        },
        {
          "name": "GCP_STAGING_APP_NAME",
          "description": "The application name for staging env (only define to override default)",
          "advanced": true
        },
        {
          "name": "GCP_STAGING_ENVIRONMENT_URL",
          "type": "url",
          "description": "The staging environment url _(only define for static environment URLs declaration and if different from default)_",
          "advanced": true
        },
        {
          "name": "GCP_STAGING_KEY_FILE",
          "description": "Service Account key file to authenticate on staging env (only define to override default)",
          "secret": true
        }
      ]
    },
    {
      "id": "prod",
      "name": "Production",
      "description": "The production environment",
      "variables": [
        {
          "name": "GCP_PROD_PROJECT",
          "description": "Google Cloud project ID for production env",
          "mandatory": true
        },
        {
          "name": "GCP_PROD_APP_NAME",
          "description": "The application name for production env (only define to override default)",
          "advanced": true
        },
        {
          "name": "GCP_PROD_ENVIRONMENT_URL",
          "type": "url",
          "description": "The production environment url _(only define for static environment URLs declaration and if different from default)_",
          "advanced": true
        },
        {
          "name": "GCP_PROD_DEPLOY_STRATEGY",
          "description": "Defines the deployment to production strategy.",
          "type": "enum",
          "values": ["manual", "auto"],
          "default": "manual"
        },
        {
          "name": "GCP_PROD_KEY_FILE",
          "description": "Service Account key file to authenticate on production env (only define to override default)",
          "secret": true
        }
      ]
    }
  ],
  "variants": [
    {
      "id": "oidc",
      "name": "OpenID Connect",
      "description": "Enables [federated authentication using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/)",
      "template_path": "templates/gitlab-ci-gcloud-oidc.yml",
      "variables": [
        {
          "name": "GCP_OIDC_AUD",
          "description": "The `aud` claim for the JWT",
          "default": "$CI_SERVER_URL"
        },
        {
          "name": "GCP_OIDC_PROVIDER",
          "description": "Global Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/)",
          "advanced": true
        },
        {
          "name": "GCP_OIDC_ACCOUNT",
          "description": "Global Service Account to which impersonate with OpenID Connect authentication",
          "advanced": true
        },
        {
          "name": "GCP_REVIEW_OIDC_PROVIDER",
          "description": "Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) on `review` environment\n\n_(only define to override default)_",
          "advanced": true
        },
        {
          "name": "GCP_REVIEW_OIDC_ACCOUNT",
          "description": "Service Account to which impersonate with OpenID Connect authentication on `review` environment",
          "advanced": true
        },
        {
          "name": "GCP_INTEG_OIDC_PROVIDER",
          "description": "Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) on `integration` environment\n\n_(only define to override default)_",
          "advanced": true
        },
        {
          "name": "GCP_INTEG_OIDC_ACCOUNT",
          "description": "Service Account to which impersonate with OpenID Connect authentication on `integration` environment",
          "advanced": true
        },
        {
          "name": "GCP_STAGING_OIDC_PROVIDER",
          "description": "Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) on `staging` environment\n\n_(only define to override default)_",
          "advanced": true
        },
        {
          "name": "GCP_STAGING_OIDC_ACCOUNT",
          "description": "Service Account to which impersonate with OpenID Connect authentication on `staging` environment",
          "advanced": true
        },
        {
          "name": "GCP_PROD_OIDC_PROVIDER",
          "description": "Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) on `production` environment\n\n_(only define to override default)_",
          "advanced": true
        },
        {
          "name": "GCP_PROD_OIDC_ACCOUNT",
          "description": "Service Account to which impersonate with OpenID Connect authentication on `production` environment",
          "advanced": true
        }
      ]
    },
    {
      "id": "vault",
      "name": "Vault",
      "description": "Retrieve secrets from a [Vault](https://www.vaultproject.io/) server",
      "template_path": "templates/gitlab-ci-gcloud-vault.yml",
      "variables": [
        {
          "name": "TBC_VAULT_IMAGE",
          "description": "The [Vault Secrets Provider](https://gitlab.com/to-be-continuous/tools/vault-secrets-provider) image to use",
          "default": "registry.gitlab.com/to-be-continuous/tools/vault-secrets-provider:latest",
          "advanced": true
        },
        {
          "name": "VAULT_OIDC_AUD",
          "description": "The `aud` claim for the JWT",
          "default": "$CI_SERVER_URL"
        },
        {
          "name": "VAULT_BASE_URL",
          "description": "The Vault server base API url",
          "mandatory": true
        },
        {
          "name": "VAULT_ROLE_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID",
          "mandatory": true,
          "secret": true
        },
        {
          "name": "VAULT_SECRET_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID",
          "mandatory": true,
          "secret": true
        }
      ]
    }
  ]
}
